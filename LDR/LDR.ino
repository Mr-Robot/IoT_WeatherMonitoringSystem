//positive to R1, negative to LDR

float Vcc = 3.3;
int n = 10;
int R1 = 5400;
int Vin = 3.3;

void setup() {

  Serial.begin(9600);

  

}

void loop() {

  float LDR_voltage = analogRead(A0);

  //Serial.println(LDR_voltage);

  float step_size = Vcc / (pow(2,n)-1);

  LDR_voltage = LDR_voltage * step_size;

  //Serial.println(LDR_voltage);

  float LDR_resistance = LDR_voltage * R1 / (Vin - LDR_voltage);

  //Serial.println(LDR_resistance);

  //float lux = -0.0688*LDR_resistance + 342.31; //all data, doesn't work

  //float lux = -0.0127 * LDR_resistance + 80.547; // up to 60 lux

  float LDR_resistanceKohms = (LDR_resistance/1000) * 10;
  float luxlog = pow(LDR_resistanceKohms,1.75);
  luxlog = 10000 / luxlog;

  float lux = (LDR_resistance - 3118.1 )/ -4.024; // works from approx 200 - 800 lux

  Serial.println("Lux from Excel:");
  Serial.println(lux);

  Serial.println("Lux from Log Formula:");
  Serial.println(luxlog);

  delay(1000);
  

}
