#include <ESP8266WiFi.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

Adafruit_BMP280 bme; //I2C

const char* ssid     = "B00089231";
const char* password = "B00089231";
const char* host = "192.168.43.201";

void connectWiFi(); // establishes connection to internet and prints status to serial port @115200

float Vcc = 3.3;
int n = 10;
int R1 = 5400;
int Vin = 3.3;

const byte ADDRESS = 0x27; // Humidity Sensor

float calculateLux();
float calculatePressure();
float calculateHumidity();
float calculateTemperature();

void setup() {

  connectWiFi();

  Wire.begin(2,0); // (SDA,SCL) NodeMCU = (D4,D3)
  
  Serial.begin(115200);
  
  if (!bme.begin()) {  
    Serial.println("Could not find a valid BMP280 sensor, check wiring!");
  }
  
  // Give sensors time to start up 
  delay( 5000 );
}

void loop() {

  float pressure = calculatePressure();

  float lux = calculateLux();

  float humidity = calculateHumidity();

  float temperature = calculateTemperature();

  WiFiClient client; //initialize the client library
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) { //establish connection with raspberry pi
    Serial.println("connection failed");
    return;
  }

  String url = "/write.php?sensor_Id=1234&data="; // temperature sensor
  
  url += String(temperature,4); // convert float temeprature to string

  Serial.print(url);

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");

  delay(1000);

  Serial.println();
  Serial.println("closing connection");

  if (!client.connect(host, httpPort)) { //establish connection with raspberry pi
    Serial.println("connection failed");
    return;
  }

  url = "/write.php?sensor_Id=1357&data="; // humidity sensor
  
  url += String(humidity,4); // convert float temeprature to string

  Serial.print(url);

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");

  delay(1000);

  Serial.println();
  Serial.println("closing connection");

    if (!client.connect(host, httpPort)) { //establish connection with raspberry pi
    Serial.println("connection failed");
    return;
  }

  url = "/write.php?sensor_Id=2468&data="; // light sensor
  
  url += String(lux,4); // convert float temeprature to string

  Serial.print(url);

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");

  delay(1000);

  Serial.println();
  Serial.println("closing connection");

    if (!client.connect(host, httpPort)) { //establish connection with raspberry pi
    Serial.println("connection failed");
    return;
  }

  url = "/write.php?sensor_Id=5678&data="; //pressure sensor
  
  url += String(pressure,4); // convert float temeprature to string

  Serial.print(url);

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");

  delay(1000);

  Serial.println();
  Serial.println("closing connection");


  delay(1000);

}


float calculateLux(){

  float LDR_voltage = analogRead(A0); // read LDR pin

  float step_size = Vcc / (pow(2,n)-1); // calculate step size

  LDR_voltage = LDR_voltage * step_size; // convert back to voltage voltage

  float LDR_resistance = LDR_voltage * R1 / (Vin - LDR_voltage); // voltage divider

  float lux = (LDR_resistance - 3118.1 )/ -4.024;

  if(lux<=60){
    lux = -0.0127 * LDR_resistance + 80.547; //use lower band formula
  }

  if(lux<0){
    lux =0; //prevent negative values
  }

  return lux;
  
}


float calculatePressure(){

  float pressure = bme.readPressure(); // read sensor

  pressure  = pressure / 100; // convert to hPa

  return pressure;
  
}

float calculateHumidity(){

  byte hhigh;
  byte hlow;
  byte state;
  byte thigh;
  byte tlow;

  // Let the sensor know we are coming
  Wire.beginTransmission( ADDRESS ); 
  Wire.endTransmission();
  delay( 100 );
      
  // Read the data packets
  Wire.requestFrom( (int)ADDRESS, 4 );
  hhigh = Wire.read();
  hlow = Wire.read();
  thigh = Wire.read();
  tlow = Wire.read();
  Wire.endTransmission();
      
  // Slice of state bytes
  state = ( hhigh >> 6 ) & 0x03;
  
  // Clean up remaining humidity bytes
  hhigh = hhigh & 0x3f;
  
  // Shift humidity bytes into a value
  // Convert value to humidity per data sheet  
  float hdata = ( ( (unsigned int)hhigh ) << 8 ) | hlow;
  hdata = hdata * 6.10e-3;
      

  return hdata;
 
}

float calculateTemperature(){

  return bme.readTemperature();
}

void connectWiFi(){

  Serial.begin(115200);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Netmask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway: ");
  Serial.println(WiFi.gatewayIP());

  delay(5000);
  
}

