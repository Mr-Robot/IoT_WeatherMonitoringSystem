#include <Wire.h>


byte address = 0x27; // default address for I2C

void setup() {
  
Wire.begin(); //Join I2C Bus

Serial.begin(9600);


}

void loop() {

  
  unsigned int data[4];

  Wire.beginTransmission(address);

  Wire.write(0); // issue Measure Command

  Wire.requestFrom(address,4);

  if(Wire.available() == 0)
  {
    data[0] = Wire.read();
    data[1] = Wire.read();
    data[2] = Wire.read();
    data[3] = Wire.read();

    float humidity = (((data[0] & 0x3F) * 256.0) +  data[1]) * (100.0 / 16383.0);
    float cTemp = (((data[2] * 256.0) + (data[3] & 0xFC)) / 4) * (165.0 / 16383.0) - 40;

     Wire.endTransmission();

  }

  else
  {
    Serial.println("Error reading");
  }


}
