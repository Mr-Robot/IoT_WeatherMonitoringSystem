<!DOCTYPE html>
<html>

<head>

    <title>
        IoT Weather Monitoring System
    </title>

    <link rel="stylesheet" type="text/css" href="styles.css" />
    <script src="myscripts.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6XpWK94h0p8UHJA-pUWgWKoMSJRFFkGE&callback=initMap">
    </script>

    <script>
        function initMap() {
            var uluru = {
                lat: 53.404743,
                lng: -6.378090
            };
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 18,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
    </script>

    <style>

      #information{
        background-color: red;
        height: 100%;
        width: 50%;
        position: absolute;
      }

      .card {
        background-color: pink;
        height: 80%;
        padding: 10%;
      }

      #p_image{
        background-color: #aaa;
        width: 60%;
        height: 60%;
      }
    </style>


</head>



<body>
    <div class="container">
        <div class="mainblock">
            <div class="banner">
                <div>
                    <a href="main.php">
                        <img src="images/logo.png" class="bannerimg" />
                    </a>
                </div>
                <div id="bannertext" style="font-style:italic"> About </div>
            </div>
            <div id="mapcontainer">
                <div id="information">

                  <div class="card">
                    <h2>IoT Weather Monitoring System</h2>
                    <h5>Sensor Board</h5>
                      <img id="p_image" src="images/project.jpg"/>

                    <p>This project was created by Glenn Tarpey, student number B00089231</p>
                    <p>This sensor board uploads data to the database which is in turn displayed on this website</p>
                  </div>


                </div>

                <div id="map">

                </div>

            </div>

        </div>

        <div class="secondblock">

          <table class="table">

              <tr>
                  <td class="cell_1" id="cell1" onmouseover="change_1()" onmouseout="changeback()" onclick="location.href='loginpage.php';">

                      Log-in

                  </td>

              </tr>


              <tr>
                  <td class="cell_2" id="cell2" onmouseover="change_2()" onmouseout="changeback()" onclick="location.href='register.php';">

                        Register

                  </td>

              </tr>

              <tr>

                <td class="cell_3" id="cell3" onmouseover="change_3()" onmouseout="changeback()" onclick="location.href='mysensors.php';" >

                    My Sensors

                </td>

            </tr>

            <tr>

                <td class="cell_4" id="cell4" onmouseover="change_4()" onmouseout="changeback()" onclick="location.href='about.php';">

                    About

                </td>

              </tr>

          </table>


        </div>


    </div>



</body>



</html>
