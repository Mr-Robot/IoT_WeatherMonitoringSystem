function change_1() {
    document.getElementById("cell1").className = "cell_1_hover";
}

function change_2() {
    document.getElementById("cell2").className = "cell_2_hover";
}

function change_3() {
    document.getElementById("cell3").className = "cell_3_hover";
}

function change_4() {
    document.getElementById("cell4").className = "cell_4_hover";
}

function changeback() {
    document.getElementById("cell1").className = "cell_1";
    document.getElementById("cell2").className = "cell_2";
    document.getElementById("cell3").className = "cell_3";
    document.getElementById("cell4").className = "cell_4";
}

function tabclick1() {
    tabwhite();
    document.getElementById("tab1").className = "tabclick";
}

function tabclick2() {
    tabwhite();
    document.getElementById("tab2").className = "tabclick";
}

function tabclick3() {
    tabwhite();
    document.getElementById("tab3").className = "tabclick";
}

function tabclick4() {
    tabwhite();
    document.getElementById("tab4").className = "tabclick";
}

function tabwhite() {
    document.getElementById("tab1").className = "tab";
    document.getElementById("tab2").className = "tab";
    document.getElementById("tab3").className = "tab";
    document.getElementById("tab4").className = "tab";

}

function tabclick1exam() {
    tabwhite();
    document.getElementById("tab1").className = "tabclickexam";
}

function tabclick2exam() {
    tabwhite();
    document.getElementById("tab2").className = "tabclickexam";
}

function tabclick3exam() {
    tabwhite();
    document.getElementById("tab3").className = "tabclickexam";
}

function tabclick4exam() {
    tabwhite();
    document.getElementById("tab4").className = "tabclickexam";
}

function goLogin() {
    setTimeout(function()
    { window.location.assign("loginpage.php")}, 3000);
}

function notLoggedIn(){

  $("notification").ready(function(){

    $.notify("Not Logged In, Redirecting now","info");
  });
}

$(document).ready(function(){
    startUp();
});



function startUp(){
  clearAll();
  showRealTime();
}


// The following code has been referenced from : https://codepen.io/developer007/pen/NNdBQwc

    function thermometer(goalAmount, progressAmount, animate) {
      "use strict";
      var $thermo = $("#thermometer"),
          $progress = $(".progress", $thermo),
          $goal = $(".goal", $thermo),
          percentageAmount;

      goalAmount = goalAmount || parseFloat($goal.text()),
      progressAmount = progressAmount || parseFloat($progress.text()),
      percentageAmount = Math.min(Math.round(progressAmount / goalAmount * 1000) / 10, 100); //make sure we have 1 decimal point

      $goal.find(".amount").text();
      $progress.find(".amount").text();

      $progress.find(".amount").hide();
      if (animate !== false) {
          $progress.animate({
              "height": percentageAmount + "%"
          }, 1200, function () {
              $(this).find(".amount").fadeIn(200);
          });
      } else {
          $progress.css({
              "height": percentageAmount + "%"
          });
          $progress.find(".amount").fadeIn(200);
      }
  }

  $(document).ready(function () {

      thermometer();

  });

  // End of referenced code

////////////////////////////////////////////////////////
  $(document).ready(function(){
      $("#cell1").click(function(){ //Real-Time
          clearAll();
          showRealTime();
      });
  });

  $(document).ready(function(){
      $("#cell2").click(function(){ //Historical
          clearAll();
          showHistorical();
      });
  });

  $(document).ready(function(){
      $("#cell3").click(function(){ //Future
          clearAll();
          showFuture();
      });
  });

  $(document).ready(function(){
      $("#cell4").click(function(){ //Alert
          clearAll();
          showAlert();
      });
  });

  function clearAll(){
    clearRealTime();
    clearHistorical();        // Add These as you go
    clearFuture();
    clearAlert();
  }

  function clearRealTime(){
      $("#leftblock").hide();
      $("#rightblock").hide();
  }

  function clearHistorical(){
      $("#chartarea").hide();
  }

  function clearFuture(){
      $("#future").hide();
  }

  function clearAlert(){
      $("#alert").hide();
  }

  function showRealTime(){
    $("#leftblock").show();
    $("#rightblock").show();
  }

  function showHistorical(){
    $("#chartarea").show();
    drawGraph();
  }

  function showFuture(){
    $("#future").show();
  }

  function showAlert(){
    $("#alert").show();
  }


// Clock Functions
//////////////////////////////////////////////////////////////////////////
  function updateClock(){
    var d = new Date();
    document.getElementById("timeboxtext").innerHTML = d.toUTCString();
  }

  $(document).ready(function(){
     setInterval('updateClock()', 1000);
  });
/////////////////////////////////////////////////////////////////////////

function drawGraph() {
  var data = google.visualization.arrayToDataTable([
    ['Day', 'Temperature'],
    ['Monday',  10],
    ['Tuesday',  12],
    ['Wednesday',  9],
    ['Thursday',  15],
    ['Friday',  18],
    ['Saturday',  19],
    ['Sunday',  -2]
  ]);

  var options = {
    title: 'Weather from last week',
    hAxis: {title: 'Day',  titleTextStyle: {color: '#333'}},
    vAxis: {title: 'Temperature: Celsius',minValue: 0}
  };

  document.getElementById('chartarea').className='show';
  var chart = new google.visualization.AreaChart(document.getElementById('chartarea'));
  chart.draw(data, options);

}
