<?php
session_start();
?>

<!DOCTYPE html>
<html>

<head>

  <link rel="stylesheet" type="text/css" href="styles.css" />
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
  <script src="myscripts.js"></script>
  <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="js/notify.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script> google.charts.load('current', {'packages':['corechart']}); </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src='https://cdn.rawgit.com/monkeecreate/jquery.simpleWeather/master/jquery.simpleWeather.min.js'></script>
  <script  src="js/simpleWeather.js"></script>

  <?php

  $db = mysqli_connect('localhost','root','raspberry','IoT_Sensor_System')
  or die('Error connecting to database');
  ?>

  <script>
  function goLogin() {
      setTimeout(function()
      { window.location.assign("loginpage.php")}, 3000);
  }

  function notLoggedIn(){

    $("notification").ready(function(){

      $.notify("Not Logged In, Redirecting now","info");
    });
  }
  </script>

  <?php
  if (isset($_SESSION['username']) && $_SESSION['password'] == true) {

    $username = $_SESSION['username'];
    $password = $_SESSION['password'];

    // Get the Temperature ID of the user's Sensor

    $query = "SELECT sensor_Id
              FROM user
              NATURAL JOIN sensor
              WHERE sensor.type='temperature' && email='$username' ";

    mysqli_query($db, $query) or die('Error querying database');

    $result = mysqli_query($db, $query);

    while($row = mysqli_fetch_array($result)){
    	$temperature_Id = $row["sensor_Id"];
    }

    // Get the Pressure ID of the user's Sensor

    $query = "SELECT sensor_Id
              FROM user
              NATURAL JOIN sensor
              WHERE sensor.type='pressure' && email='$username' ";

    mysqli_query($db, $query) or die('Error querying database');

    $result = mysqli_query($db, $query);

    while($row = mysqli_fetch_array($result)){
    	$pressure_Id = $row["sensor_Id"];
    }

    // Get the Humidity ID of the user's Sensor

    $query = "SELECT sensor_Id
              FROM user
              NATURAL JOIN sensor
              WHERE sensor.type='humidity' && email='$username' ";

    mysqli_query($db, $query) or die('Error querying database');

    $result = mysqli_query($db, $query);

    while($row = mysqli_fetch_array($result)){
    	$humidity_Id = $row["sensor_Id"];
    }

    // Get the Light ID of the user's Sensor

    $query = "SELECT sensor_Id
              FROM user
              NATURAL JOIN sensor
              WHERE sensor.type='light' && email='$username' ";

    mysqli_query($db, $query) or die('Error querying database');

    $result = mysqli_query($db, $query);

    while($row = mysqli_fetch_array($result)){
    	$light_Id = $row["sensor_Id"];
    }

  //  echo $temperature_Id."<br />";
  //  echo $pressure_Id."<br />";
  //  echo $light_Id."<br />";
  //  echo $humidity_Id."<br />";

    // Getting the latest sensor data

    $query = "SELECT data FROM `sensor_data`
              WHERE `sensor_Id` = $temperature_Id
              ORDER BY time DESC
              LIMIT 1";

    mysqli_query($db, $query) or die('Error querying database');

    $result = mysqli_query($db, $query);

    while($row = mysqli_fetch_array($result)){
    	$temperature_Realtime = $row["data"];
    }

    //Pressure

    $query = "SELECT data FROM `sensor_data`
              WHERE `sensor_Id` = $pressure_Id
              ORDER BY time DESC
              LIMIT 1";

    mysqli_query($db, $query) or die('Error querying database');

    $result = mysqli_query($db, $query);

    while($row = mysqli_fetch_array($result)){
    	$pressure_Realtime = $row["data"];
    }

    //humidity

    $query = "SELECT data FROM `sensor_data`
              WHERE `sensor_Id` = $humidity_Id
              ORDER BY time DESC
              LIMIT 1";

    mysqli_query($db, $query) or die('Error querying database');

    $result = mysqli_query($db, $query);

    while($row = mysqli_fetch_array($result)){
    	$humidity_Realtime = $row["data"];
    }

    //light

    $query = "SELECT data FROM `sensor_data`
              WHERE `sensor_Id` = $light_Id
              ORDER BY time DESC
              LIMIT 1";

    mysqli_query($db, $query) or die('Error querying database');

    $result = mysqli_query($db, $query);

    while($row = mysqli_fetch_array($result)){
    	$light_Realtime = $row["data"];
    }

  } else {
      echo "<script>notLoggedIn();</script>";
      echo "<script>goLogin();</script>";
    }
   ?>

  <script>

  $(document).ready(function(){
      startUp();
  });

  function startUp(){
    clearAll();
    showRealTime();
  }


  // The following code has been referenced from : https://codepen.io/developer007/pen/NNdBQwc

      function thermometer(goalAmount, progressAmount, animate) {
        "use strict";
        var $thermo = $("#thermometer"),
            $progress = $(".progress", $thermo),
            $goal = $(".goal", $thermo),
            percentageAmount;

        goalAmount = goalAmount || parseFloat($goal.text()),
        progressAmount = progressAmount || parseFloat($progress.text()),
        percentageAmount = Math.min(Math.round(progressAmount / goalAmount * 1000) / 10, 100); //make sure we have 1 decimal point

        $goal.find(".amount").text();
        $progress.find(".amount").text();

        $progress.find(".amount").hide();
        if (animate !== false) {
            $progress.animate({
                "height": percentageAmount + "%"
            }, 1200, function () {
                $(this).find(".amount").fadeIn(200);
            });
        } else {
            $progress.css({
                "height": percentageAmount + "%"
            });
            $progress.find(".amount").fadeIn(200);
        }
    }

    $(document).ready(function () {

        thermometer();

    });

    // End of referenced code

////////////////////////////////////////////////////////
    $(document).ready(function(){
        $("#cell1").click(function(){ //Real-Time
            clearAll();
            showRealTime();
        });
    });

    $(document).ready(function(){
        $("#cell2").click(function(){ //Historical
            clearAll();
            showHistorical();
        });
    });

    $(document).ready(function(){
        $("#cell3").click(function(){ //Future
            clearAll();
            showFuture();
        });
    });

    $(document).ready(function(){
        $("#cell4").click(function(){ //Alert
            clearAll();
            showAlert();
        });
    });

    function clearAll(){
      clearRealTime();
      clearHistorical();        // Add These as you go
      clearFuture();
      clearAlert();
    }

    function clearRealTime(){
        $("#leftblock").hide();
        $("#rightblock").hide();
    }

    function clearHistorical(){
        $("#chartarea").hide();
    }

    function clearFuture(){
        $("#future").hide();
    }

    function clearAlert(){
        $("#alert").hide();
    }

    function showRealTime(){
      $("#leftblock").show();
      $("#rightblock").show();
    }

    function showHistorical(){
      $("#chartarea").show();
      drawGraph();
    }

    function showFuture(){
      $("#future").show();
    }

    function showAlert(){
      $("#alert").show();
    }


// Clock Functions
//////////////////////////////////////////////////////////////////////////
    function updateClock(){
      var d = new Date();
      document.getElementById("timeboxtext").innerHTML = d.toUTCString();
    }

    $(document).ready(function(){
       setInterval('updateClock()', 1000);
    });
/////////////////////////////////////////////////////////////////////////

  function drawGraph() {
    var data = google.visualization.arrayToDataTable([
      ['Day', 'Temperature'],
      ['Monday',  10],
      ['Tuesday',  12],
      ['Wednesday',  9],
      ['Thursday',  15],
      ['Friday',  18],
      ['Saturday',  19],
      ['Sunday',  -2]
    ]);

    var options = {
      title: 'Weather from last week',
      hAxis: {title: 'Day',  titleTextStyle: {color: '#333'}},
      vAxis: {title: 'Temperature: Celsius',minValue: 0}
    };

    document.getElementById('chartarea').className='show';
    var chart = new google.visualization.AreaChart(document.getElementById('chartarea'));
    chart.draw(data, options);

  }



  </script>


    <title>
        IoT Weather Monitoring System
    </title>


</head>

<body>
        <div class="mainblock">
            <div class="banner">
                <div>
                    <a href="main.php">
                        <img src="images/logo.png" class="bannerimg" />
                    </a>
                </div>
                <div id="bannertext" style="font-style:italic"> IoT Weather Monitoring System </div>
            </div>
            <div class="body" id="body">

               <div id="leftblock">

<!-- The following code has been referenced from : https://codepen.io/developer007/pen/NNdBQwc -->
                 <div id="thermometer">
                            <div class="track">

                              <div class="goal">
                                  <div class="amount" id="goalAmount">35°C</div>
                              </div>

                              <div class="progress">
                                  <div style="display: block;" class="amount"   id="progessAmount"><?php echo $temperature_Realtime."°C"; ?></div>
                              </div>

                  		        <div class="bulb">
                                <div class="inner-bulb">

                                </div>

                              </div>
                            </div>
                  </div>

<!-- End of referenced code -->



               </div>

               <div id="rightblock">
                 <div id="timebox">
                   <div id="timeboxtext">

                   </div>
                 </div>

                 <div id="humidity">

                    Relative Humidity:

                   <div id="humiditytext"> <?php echo $humidity_Realtime." %"; ?></div>

                 </div>

                 <div id="lux">

                   Light Intensity:

                   <div id="luxtext"> <?php echo $light_Realtime." lux"; ?></div>

                 </div>

                 <div id="pressure">

                   Atmospheric Pressure (hPa) :

                   <div id="pressuretext"> <?php echo $pressure_Realtime; ?> </div>

                 </div>



               </div>


              <div id="chartarea">


              </div>

              <div id="future">

                <div id="weather"></div>


              </div>

              <div id="alert">

              </div>
            </div>


          </div>
        <div class="secondblock">

          <table class="table">

              <tr>
                  <td class="cell_1" id="cell1" onmouseover="change_1()" onmouseout="changeback()" >

                      Real-Time

                  </td>

              </tr>


              <tr>
                  <td class="cell_2" id="cell2" onmouseover="change_2()" onmouseout="changeback()" >

                    Historical

                  </td>

              </tr>

              <tr>

                <td class="cell_3" id="cell3" onmouseover="change_3()" onmouseout="changeback()"  >

                    Future

                </td>

            </tr>

            <tr>

                <td class="cell_4" id="cell4" onmouseover="change_4()" onmouseout="changeback()">

                    Alerts

                </td>

              </tr>

          </table>


        </div>



    </div>


</body>



</html>
