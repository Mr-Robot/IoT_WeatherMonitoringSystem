<?php

$db = mysqli_connect('localhost','root','raspberry','IoT_Sensor_System')
or die('Error connecting to database');
?>

<html>
<head>
</head>
<body>

<?php

  $sixdaysago = date('l',strtotime(date('d.m.Y',strtotime("-6 days")))); // returns day from 6 days ago
  $fivedaysago = date('l',strtotime(date('d.m.Y',strtotime("-5 days")))); // returns day from 5 days ago
  $fourdaysago = date('l',strtotime(date('d.m.Y',strtotime("-4 days")))); // returns day from 4 days ago
  $threedaysago = date('l',strtotime(date('d.m.Y',strtotime("-3 days")))); // returns day from 3 days ago
  $twodaysago = date('l',strtotime(date('d.m.Y',strtotime("-2 days")))); // returns day from 2 days ago
  $onedaysago = date('l',strtotime(date('d.m.Y',strtotime("-1 days")))); // returns day from 1 days ago
  $today = date('l',strtotime(date('d.m.Y',strtotime("now"))));//returns today's day

  $sixdaysagodate = date('Y-m-d',strtotime(date('d.m.Y',strtotime("-6 days")))); // returns day from 6 days ago
  $fivedaysagodate = date('Y-m-d',strtotime(date('d.m.Y',strtotime("-5 days")))); // returns day from 5 days ago
  $fourdaysagodate = date('Y-m-d',strtotime(date('d.m.Y',strtotime("-4 days")))); // returns day from 4 days ago
  $threedaysagodate = date('Y-m-d',strtotime(date('d.m.Y',strtotime("-3 days")))); // returns day from 3 days ago
  $twodaysagodate = date('Y-m-d',strtotime(date('d.m.Y',strtotime("-2 days")))); // returns day from 2 days ago
  $onedaysagodate = date('Y-m-d',strtotime(date('d.m.Y',strtotime("-1 days")))); // returns day from 1 days ago
  $todaydate = date('Y-m-d',strtotime(date('d.m.Y',strtotime("now"))));//returns today's day


  $query = "SELECT MAX(data)
            FROM `sensor_data`
            WHERE DATE(time)='$todaydate'&&sensor_Id=1234";

  mysqli_query($db, $query) or die('Error querying database');

  $result = mysqli_query($db, $query);

  $row = mysqli_fetch_array($result);
  $todaytemperature =  $row["MAX(data)"];

  $query = "SELECT MAX(data)
            FROM `sensor_data`
            WHERE DATE(time)='$onedaysagodate'&&sensor_Id=1234";

  mysqli_query($db, $query) or die('Error querying database');

  $result = mysqli_query($db, $query);

  $row = mysqli_fetch_array($result);
  $onedaysagotemperature =  $row["MAX(data)"];

  $query = "SELECT MAX(data)
            FROM `sensor_data`
            WHERE DATE(time)='$twodaysagodate'&&sensor_Id=1234";

  mysqli_query($db, $query) or die('Error querying database');

  $result = mysqli_query($db, $query);

  $row = mysqli_fetch_array($result);
  $twodaysagotemperature =  $row["MAX(data)"];

  $query = "SELECT MAX(data)
            FROM `sensor_data`
            WHERE DATE(time)='$threedaysagodate'&&sensor_Id=1234";

  mysqli_query($db, $query) or die('Error querying database');

  $result = mysqli_query($db, $query);

  $row = mysqli_fetch_array($result);
  $threedaysagotemperature =  $row["MAX(data)"];

  $query = "SELECT MAX(data)
            FROM `sensor_data`
            WHERE DATE(time)='$fourdaysagodate'&&sensor_Id=1234";

  mysqli_query($db, $query) or die('Error querying database');

  $result = mysqli_query($db, $query);

  $row = mysqli_fetch_array($result);
  $fourdaysagotemperature =  $row["MAX(data)"];

  $query = "SELECT MAX(data)
            FROM `sensor_data`
            WHERE DATE(time)='$fivedaysagodate'&&sensor_Id=1234";

  mysqli_query($db, $query) or die('Error querying database');

  $result = mysqli_query($db, $query);

  $row = mysqli_fetch_array($result);
  $fivedaysagotemperature =  $row["MAX(data)"];

  $query = "SELECT MAX(data)
            FROM `sensor_data`
            WHERE DATE(time)='$sixdaysagodate'&&sensor_Id=1234";

  mysqli_query($db, $query) or die('Error querying database');

  $result = mysqli_query($db, $query);

  $row = mysqli_fetch_array($result);
  $sixdaysagotemperature =  $row["MAX(data)"];

  $dayofweek = array($sixdaysago,$fivedaysago,$fourdaysago,$threedaysago,$twodaysago,$onedaysago,$today);

  $maxtemperatures = array($sixdaysagotemperature,$fivedaysagotemperature,$fourdaysagotemperature,$threedaysagotemperature,$twodaysagotemperature,$onedaysagotemperature,$todaytemperature);

/*
  echo $sixdaysagodate." : ".$sixdaysagotemperature."<br>";
  echo $fivedaysagodate." : ".$fivedaysagotemperature."<br>";
  echo $fourdaysagodate." : ".$fourdaysagotemperature."<br>";
  echo $threedaysagodate." : ".$threedaysagotemperature."<br>";
  echo $twodaysagodate." : ".$twodaysagotemperature."<br>";
  echo $onedaysagodate." : ".$onedaysagotemperature."<br>";
  echo $todaydate." : ".$todaytemperature."<br>";
  */

  /*
    echo $sixdaysago;
    echo $fivedaysago;
    echo $fourdaysago;
    echo $threedaysago;
    echo $twodaysago;
    echo $onedaysago;
    echo $today;
  */


?>

</body>
</html>
