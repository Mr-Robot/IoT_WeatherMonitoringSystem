<!DOCTYPE html>
<html>

<head>

    <title>
        IoT Weather Monitoring System
    </title>

    <link rel="stylesheet" type="text/css" href="styles.css" />
    <script src="myscripts.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>

    var check = function() {
      if (document.getElementById('password').value ==
        document.getElementById('passwordcheck').value) {
        document.getElementById('message').style.color = 'green';
        document.getElementById('message').innerHTML = 'Passwords Match';
      } else {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'Not Matching';
      }
    }

    function match(){
        var email = document.getElementById('email').value;
        var password = document.getElementById('password').value;
        var passwordcheck = document.getElementById('passwordcheck').value;
        var status = null;

        if(email.includes("@") && password === passwordcheck){
            status = true;
        }
        else {
          window.alert("form invalid");
          status = false;
        }

        return status;
    }

    </script>

</head>

<body>
        <div class="mainblock">
            <div class="banner">
                <div>
                    <a href="main.php">
                        <img src="images/logo.png" class="bannerimg" />
                    </a>
                </div>
                <div id="bannertext" style="font-style:italic"> IoT Weather Monitoring System </div>
            </div>
            <div class="body" id="body">
              <form class="signup" action="newuser.php" method="get" onsubmit="return match()">
                  <h1>Register</h1>
                  <p>Please fill in this form to create an account.</p>
                  <hr>

                  <input type="text" placeholder="Enter Email" name="email" id="email" required>


                  <input type="password" placeholder="Enter Password" name="password" id="password" onkeyup='check();' required>


                  <input type="password" placeholder="Re-Enter Password" name="passwordcheck" id="passwordcheck" onkeyup='check();' required>

                  <div id='message' style="clear:left"></div>


                  <button type="submit" class="signupbtn">Sign Up</button>
              </form>
            </div>


          </div>
        <div class="secondblock">

          <table class="table">

              <tr>
                  <td class="cell_1" id="cell1" onmouseover="change_1()" onmouseout="changeback()" onclick="location.href='loginpage.php';">

                      Log-in

                  </td>

              </tr>


              <tr>
                  <td class="cell_2" id="cell2" onmouseover="change_2()" onmouseout="changeback()" onclick="location.href='register.php';">

                        Register

                  </td>

              </tr>

              <tr>

                <td class="cell_3" id="cell3" onmouseover="change_3()" onmouseout="changeback()" onclick="location.href='mysensors.php';" >

                    My Sensors

                </td>

            </tr>

            <tr>

                <td class="cell_4" id="cell4" onmouseover="change_4()" onmouseout="changeback()" onclick="location.href='about.php';">

                    About

                </td>

              </tr>

          </table>


        </div>



    </div>



</body>



</html>
