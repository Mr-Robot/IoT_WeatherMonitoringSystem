/* Wiring for BMP280 to NodeMCU : 
 *  VCC -> 3.3v
 *  GND -> GND
 *  SCL -> D3
 *  SDA -> D4
 *  CSB -> 3.3v // tie to 3.3 for I2C , GND for SPI
 *  SDO -> 3.3v // tie to 3.3 for address 0x77
 */

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

Adafruit_BMP280 bme; // I2C

// Default pins
//SCL to pin D1 (GPIO5)
//SDA to pin D2 (GPIO4)  


void setup() {
  Serial.begin(9600);
  Serial.println("BMP280 test");

  Wire.begin(2,0); // (SDA,SCL) NodeMCU = (D4,D3)
  
  if (!bme.begin()) {  
    Serial.println("Could not find a valid BMP280 sensor, check wiring!");
    while (1);
  }
  
}

void loop() {
  
    Serial.print("T=");
    Serial.print(bme.readTemperature());
    Serial.print(" *C");
    
    Serial.print(" P=");
    Serial.print(bme.readPressure());
    Serial.print(" Pa");

    Serial.print(" A= ");
    Serial.print(bme.readAltitude(1013.25)); // this should be adjusted to your local forcase
    Serial.println(" m");


}
