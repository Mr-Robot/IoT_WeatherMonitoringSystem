#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

Adafruit_BMP280 bme; //I2C

float Vcc = 3.3;
int n = 10;
int R1 = 5400;
int Vin = 3.3;

const byte ADDRESS = 0x27; // Humidity Sensor

float calculateLux();
float calculatePressure();
float calculateHumidity();
float calculateTemperature();

void setup() {

  Wire.begin(2,0);
  
  Serial.begin(9600);
  
  if (!bme.begin()) {  
    Serial.println("Could not find a valid BMP280 sensor, check wiring!");
  }
  
  // Give sensors time to start up 
  delay( 5000 );
}

void loop() {

  float pressure = calculatePressure();

  float lux = calculateLux();

  float humidity = calculateHumidity();

  float temperature = calculateTemperature();

  Serial.println("lux:");
  Serial.println(lux);

  Serial.println("Pressure hPa:");
  Serial.println(pressure);

  Serial.println("Humidity:");
  Serial.println(humidity);

  Serial.println("Temperature:");
  Serial.println(temperature);

  delay(1000);

}


float calculateLux(){

  float LDR_voltage = analogRead(A0); // read LDR pin

  float step_size = Vcc / (pow(2,n)-1); // calculate step size

  LDR_voltage = LDR_voltage * step_size; // convert back to voltage voltage

  float LDR_resistance = LDR_voltage * R1 / (Vin - LDR_voltage); // voltage divider

  float lux = (LDR_resistance - 3118.1 )/ -4.024;

  return lux;
  
}


float calculatePressure(){

  float pressure = bme.readPressure(); // read sensor

  pressure  = pressure / 100; // convert to hPa

  return pressure;
  
}

float calculateHumidity(){

  byte hhigh;
  byte hlow;
  byte state;
  byte thigh;
  byte tlow;

  // Let the sensor know we are coming
  Wire.beginTransmission( ADDRESS ); 
  Wire.endTransmission();
  delay( 100 );
      
  // Read the data packets
  Wire.requestFrom( (int)ADDRESS, 4 );
  hhigh = Wire.read();
  hlow = Wire.read();
  thigh = Wire.read();
  tlow = Wire.read();
  Wire.endTransmission();
      
  // Slice of state bytes
  state = ( hhigh >> 6 ) & 0x03;
  
  // Clean up remaining humidity bytes
  hhigh = hhigh & 0x3f;
  
  // Shift humidity bytes into a value
  // Convert value to humidity per data sheet  
  float hdata = ( ( (unsigned int)hhigh ) << 8 ) | hlow;
  hdata = hdata * 6.10e-3;
      

  return hdata;
 
}

float calculateTemperature(){

  return bme.readTemperature();
}

