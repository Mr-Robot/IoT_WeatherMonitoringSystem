#define R2 1000
#define Alpha 0.0034
#define Beta 0.00025

void setup() {

  pinMode(A0,INPUT);

  Serial.begin(115200);

}

void loop() {

  float temperature = calculateTemperature();

  Serial.print("temperature \n");
  Serial.print(temperature);
  Serial.print("\n");

  delay(1000);


}

float calculateTemperature(){

    float value=analogRead(A0);//read ADC

    float voltage=value*(3.3/1024); //multiply by stepsize 3.3v/1023steps

    float Rth = ((3.3*R2) - (R2*voltage))/voltage; // calculate resistance from voltage divider

    float temperature = (Alpha)+(Beta)*(log(Rth/R2)); //bottom of steinhart hart equation

    temperature = pow(temperature,-1); //invert temperature

    temperature = temperature - 273.15; // convert from kelvin to celsius

    return temperature;
  
}

