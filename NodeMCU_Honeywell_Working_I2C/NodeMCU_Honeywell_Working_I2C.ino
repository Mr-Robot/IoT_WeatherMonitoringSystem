#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

float Vcc = 3.3;
int n = 10;
int R1 = 5400;
int Vin = 3.3;

const byte ADDRESS = 0x27; // Humidity Sensor


float calculateHumidity();


void setup() {

  Wire.begin(2,0); // (SDA,SCL) NodeMCU = (D4,D3)
  
  Serial.begin(9600);
  
}

void loop() {


  float humidity = calculateHumidity();

  Serial.println("Humidity:");
  Serial.println(humidity);

  delay(1000);

}






float calculateHumidity(){

  byte hhigh;
  byte hlow;
  byte state;
  byte thigh;
  byte tlow;

  // Let the sensor know we are coming
  Wire.beginTransmission( ADDRESS ); 
  Wire.endTransmission();
  delay( 100 );
      
  // Read the data packets
  Wire.requestFrom( (int)ADDRESS, 4 );
  hhigh = Wire.read();
  hlow = Wire.read();
  thigh = Wire.read();
  tlow = Wire.read();
  Wire.endTransmission();
      
  // Slice of state bytes
  state = ( hhigh >> 6 ) & 0x03;
  
  // Clean up remaining humidity bytes
  hhigh = hhigh & 0x3f;
  
  // Shift humidity bytes into a value
  // Convert value to humidity per data sheet  
  float hdata = ( ( (unsigned int)hhigh ) << 8 ) | hlow;
  hdata = hdata * 6.10e-3;
      

  return hdata;
 
}



