#include <ESP8266WiFi.h>

#define R2 1000
#define Alpha 0.0034
#define Beta 0.00025

#define LED 13 //GPIO13
 
const char* ssid     = "B00089231";
const char* password = "getityourself!35";
const char* host = "192.168.43.201";


void connectWiFi(); // establishes connection to internet and prints status to serial port @115200
float calculateTemperature(); //reads thermistor and returns temperature in celsius
 
void setup(){

  pinMode(LED,OUTPUT);

  pinMode(A0,INPUT); //Thermistor pin

  connectWiFi();
}
 
void loop(){

  float temperature = calculateTemperature();

  Serial.println();
  Serial.print("temperature = ");
  Serial.print(temperature);
  Serial.println();
  
  Serial.print("connecting to server ");
  Serial.println();

  WiFiClient client; //initialize the client library
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) { //establish connection with raspberry pi
    Serial.println("connection failed");
    return;
  }

  String url = "/write.php?data=";
  
  url += String(temperature,4); // convert float temeprature to string
  url +=("&unit=celsius"); // add in units to url

  Serial.print(url);

  digitalWrite(LED,HIGH);

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");

  digitalWrite(LED,LOW);

  delay(5000);

  Serial.println();
  Serial.println("closing connection");
}



void connectWiFi(){

  Serial.begin(115200);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Netmask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway: ");
  Serial.println(WiFi.gatewayIP());

  delay(5000);
  
}


float calculateTemperature(){

    float value=analogRead(A0);//read ADC

    float voltage=value*.00488; //multiply by stepsize 5v/1023steps

    float Rth = ((5*R2) - (R2*voltage))/voltage; // calculate resistance from voltage divider

    float temperature = (Alpha)+(Beta)*(log(Rth/R2)); //bottom of steinhart hart equation

    temperature = pow(temperature,-1); //invert temperature

    temperature = temperature - 273.15; // convert from kelvin to celsius

    return temperature;
}



